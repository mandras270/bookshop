package com.unideb.bookshop.shoppingcart.view.service;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.unideb.bookshop.book.domain.Book;
import com.unideb.bookshop.book.service.BookSearchService;
import com.unideb.bookshop.browse.view.model.BookView;
import com.unideb.bookshop.discount.service.DiscountCalculator;
import com.unideb.bookshop.discount.service.DiscountSearchService;
import com.unideb.bookshop.shoppingcart.view.model.CheckoutCart;
import com.unideb.bookshop.shoppingcart.view.model.ShoppingCartItem;

public class CheckoutCartCalculatorServiceTest {

	@Mock
	private BookSearchService bookSearchService;

	@Mock
	private DiscountSearchService discountSearchService;

	@Mock
	private DiscountCalculator discountCalculator;

	@InjectMocks
	private CheckoutCartCalculatorService underTest;

	@Before
	public void setUp() {
		underTest = new CheckoutCartCalculatorService();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testCreateCheckoutCartWithNoDisCount() {
		// GIVEN
		ShoppingCartItem shoppingCartItemMock = Mockito.mock(ShoppingCartItem.class);
		List<ShoppingCartItem> items = Arrays.asList(shoppingCartItemMock);
		BookView bookViewMock = Mockito.mock(BookView.class);
		long bookViewMockId = 1;
		Mockito.when(shoppingCartItemMock.getBook()).thenReturn(bookViewMock);
		int bookQuantity = 2;
		Mockito.when(shoppingCartItemMock.getQuantity()).thenReturn(bookQuantity);
		Mockito.when(bookViewMock.getBookId()).thenReturn(bookViewMockId);

		Mockito.when(discountSearchService.getBookDiscountByBookId(bookQuantity)).thenReturn(null);

		Book bookMock = Mockito.mock(Book.class);
		Mockito.when(bookSearchService.findOnebyId(bookViewMockId)).thenReturn(bookMock);

		long bookId = 2;
		String title = "title";
		String author = "author";
		double price = 10;
		Mockito.when(bookMock.getId()).thenReturn(bookId);
		Mockito.when(bookMock.getTitle()).thenReturn(title);
		Mockito.when(bookMock.getAuthor()).thenReturn(author);
		Mockito.when(bookMock.getPrice()).thenReturn(price);

		double totalPrice = bookQuantity * price;

		// WHEN
		CheckoutCart actual = underTest.createCheckoutCart(items);

		// THEN
		Assert.assertEquals(totalPrice, actual.getTotalPrice(), 0);
	}
}
