package com.unideb.bookshop.regularcustomer.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.unideb.bookshop.shoppingcart.view.model.CheckoutCart;
import com.unideb.bookshop.shoppingcart.view.model.CheckoutCartItem;

public class RegularCustomerPriceCalculatorTest {

	private RegularCustomerPriceCalculator underTest;

	@Before
	public void setUp() {
		underTest = new RegularCustomerPriceCalculator();
	}

	@Test
	public void testAppyRegularCustomerPricees() {
		// GIVEN
		CheckoutCartItem CheckoutCartItemMock = Mockito.mock(CheckoutCartItem.class);
		List<CheckoutCartItem> items = Arrays.asList(CheckoutCartItemMock);
		CheckoutCart checkoutCartMock = Mockito.mock(CheckoutCart.class);
		Mockito.when(checkoutCartMock.getItems()).thenReturn(items);
		Double totalPrice = 10.0;
		Mockito.when(CheckoutCartItemMock.getTotalPrice()).thenReturn(totalPrice);

		// WHEN
		underTest.appyRegularCustomerPricees(checkoutCartMock);

		// THEN
		double regularCustomerPriceExpected = 9.0;
		Mockito.verify(checkoutCartMock).setRegularCustomerPrice(regularCustomerPriceExpected);
	}

	@Test
	public void testAppyRegularCustomerPriceesWhenCheckoutCartIsEmpty() {
		// GIVEN
		List<CheckoutCartItem> items = new ArrayList<>();
		CheckoutCart checkoutCartMock = Mockito.mock(CheckoutCart.class);
		Mockito.when(checkoutCartMock.getItems()).thenReturn(items);

		// WHEN
		underTest.appyRegularCustomerPricees(checkoutCartMock);

		// THEN
		double regularCustomerPriceExpected = 0;
		Mockito.verify(checkoutCartMock).setRegularCustomerPrice(regularCustomerPriceExpected);
	}
}
