package com.unideb.bookshop.user.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.unideb.bookshop.user.domain.User;
import com.unideb.bookshop.user.repository.dao.UserDao;
import com.unideb.bookshop.user.repository.domain.UserEntity;
import com.unideb.bookshop.user.service.transform.UserEntitryTransformer;

public class UserWriteServiceTest {

	@Mock
	private UserEntitryTransformer transformer;

	@Mock
	private UserDao userDao;

	@InjectMocks
	private UserWriteService underTest;

	@Before
	public void setUp() {
		underTest = new UserWriteService();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testSave() {
		// GIVEN
		User userMock = Mockito.mock(User.class);
		UserEntity entityMock = Mockito.mock(UserEntity.class);
		Mockito.when(transformer.transformUser(userMock)).thenReturn(entityMock);

		// WHEN
		underTest.save(userMock);

		// THEN
		Mockito.verify(userDao).save(entityMock);

	}

}
