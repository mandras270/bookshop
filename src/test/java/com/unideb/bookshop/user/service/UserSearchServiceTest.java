package com.unideb.bookshop.user.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.unideb.bookshop.user.domain.User;
import com.unideb.bookshop.user.repository.dao.UserDao;
import com.unideb.bookshop.user.repository.domain.UserEntity;
import com.unideb.bookshop.user.service.transform.UserEntitryTransformer;

public class UserSearchServiceTest {

	@Mock
	private UserDao userDao;

	@Mock
	private UserEntitryTransformer transformer;

	@InjectMocks
	private UserSearchService underTest;

	@Before
	public void setUp() {
		underTest = new UserSearchService();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testFindOneByUsernameAndPassword() {
		// GIVEN
		String username = "username";
		String password = "password";
		User expected = Mockito.mock(User.class);
		UserEntity userEntityMock = Mockito.mock(UserEntity.class);
		Mockito.when(userDao.findOneByUsernameAndPassword(username, password)).thenReturn(userEntityMock);
		Mockito.when(transformer.transformUserEntity(userEntityMock)).thenReturn(expected);

		// WHEN
		User actual = underTest.findOneByUsernameAndPassword(username, password);

		// THEN
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testFindOneByEmailAndPassword() {
		// GIVEN
		String email = "email";
		String password = "password";
		User expected = Mockito.mock(User.class);
		UserEntity userEntityMock = Mockito.mock(UserEntity.class);
		Mockito.when(userDao.findOneByEmailAndPassword(email, password)).thenReturn(userEntityMock);
		Mockito.when(transformer.transformUserEntity(userEntityMock)).thenReturn(expected);

		// WHEN
		User actual = underTest.findOneByEmailAndPassword(email, password);

		// THEN
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testGetUserIdByUsernameOrEmailUsingEmailFormat() {
		// GIVEN
		String credential = "username@";
		String[] credentials = { credential };
		UserEntity userEntityMock = Mockito.mock(UserEntity.class);
		Mockito.when(userDao.findOneByEmail(credential)).thenReturn(userEntityMock);

		long userId = 1;
		Mockito.when(userEntityMock.getId()).thenReturn(userId);

		// WHEN
		long actual = underTest.getUserIdByUsernameOrEmail(credentials);

		// THEN
		Assert.assertEquals(userId, actual);
	}

	@Test
	public void testGetUserIdByUsernameOrEmailUsingUsernameFormat() {
		// GIVEN
		String credential = "username";
		String[] credentials = { credential };
		UserEntity userEntityMock = Mockito.mock(UserEntity.class);
		Mockito.when(userDao.findOneByUsername(credential)).thenReturn(userEntityMock);

		long userId = 1;
		Mockito.when(userEntityMock.getId()).thenReturn(userId);

		// WHEN
		long actual = underTest.getUserIdByUsernameOrEmail(credentials);

		// THEN
		Assert.assertEquals(userId, actual);
	}

	@Test
	public void testGetUserIdByUsernameOrEmailUsingUsernameFormatDaoReturnsNull() {
		// GIVEN
		String credential = "username";
		String[] credentials = { credential };
		Mockito.when(userDao.findOneByUsername(credential)).thenReturn(null);

		long expected = 0;

		// WHEN
		long actual = underTest.getUserIdByUsernameOrEmail(credentials);

		// THEN
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testGetUserIdByUsernameOrEmailUsingEmailFormatDaoReturnsNull() {
		// GIVEN
		String credential = "username@";
		String[] credentials = { credential };
		Mockito.when(userDao.findOneByEmail(credential)).thenReturn(null);

		long userId = 0;

		// WHEN
		long actual = underTest.getUserIdByUsernameOrEmail(credentials);

		// THEN
		Assert.assertEquals(userId, actual);
	}

}
