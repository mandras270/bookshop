package com.unideb.bookshop.authentication.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.unideb.bookshop.user.domain.User;
import com.unideb.bookshop.user.domain.UserRole;
import com.unideb.bookshop.user.service.UserSearchService;

public class EmailAndPasswordAuthenticationProviderTest {

	@Mock
	private UserSearchService userSearchService;
	@InjectMocks
	private EmailAndPasswordAuthenticationProvider underTest;

	@Before
	public void setUp() {
		underTest = new EmailAndPasswordAuthenticationProvider();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testAuthenticate() {
		// GIVEN
		Authentication authenticationMock = Mockito.mock(Authentication.class);
		Object credentials = Mockito.mock(Object.class);
		User user = Mockito.mock(User.class);
		String username = "userName@";
		String password = "password";
		Mockito.when(credentials.toString()).thenReturn(password);
		Mockito.when(authenticationMock.getName()).thenReturn(username);
		Mockito.when(authenticationMock.getCredentials()).thenReturn(credentials);
		Mockito.when(userSearchService.findOneByEmailAndPassword(Mockito.anyString(), Mockito.anyString())).thenReturn(
				user);
		Set<UserRole> set = new HashSet<>(Arrays.asList(UserRole.ROLE_USER));
		Mockito.when(user.getUserRoles()).thenReturn(set);
		List<GrantedAuthority> grantedAuths = new ArrayList<>();
		grantedAuths.add(new SimpleGrantedAuthority(UserRole.ROLE_USER.toString()));

		// WHEN
		Authentication actual = underTest.authenticate(authenticationMock);

		// THEN
		Assert.assertEquals(grantedAuths, actual.getAuthorities());
		Assert.assertEquals(username, actual.getPrincipal());
		Assert.assertEquals(password, actual.getCredentials());
	}

	@Test(expected = BadCredentialsException.class)
	public void testAuthenticateThrowsException() {
		// GIVEN
		Authentication authenticationMock = Mockito.mock(Authentication.class);
		Object credentials = Mockito.mock(Object.class);
		String username = "userName@";
		String password = "password";
		Mockito.when(credentials.toString()).thenReturn(password);
		Mockito.when(authenticationMock.getName()).thenReturn(username);
		Mockito.when(authenticationMock.getCredentials()).thenReturn(credentials);
		Mockito.when(userSearchService.findOneByEmailAndPassword(Mockito.anyString(), Mockito.anyString())).thenReturn(
				null);

		// WHEN
		underTest.authenticate(authenticationMock);

		// THEN EXCEPTION THROWN
	}

	@Test
	public void testAuthenticateReturnsNull() {
		// GIVEN
		Authentication authenticationMock = Mockito.mock(Authentication.class);
		Object credentials = Mockito.mock(Object.class);
		String username = "userName";
		String password = "password";
		Mockito.when(credentials.toString()).thenReturn(password);
		Mockito.when(authenticationMock.getName()).thenReturn(username);
		Mockito.when(authenticationMock.getCredentials()).thenReturn(credentials);

		// WHEN
		Authentication result = underTest.authenticate(authenticationMock);

		// THEN
		Assert.assertNull(result);
	}

	@Test
	public void testSupports() {
		// GIVEN

		// WHEN
		boolean result = underTest.supports(UsernamePasswordAuthenticationToken.class);

		// THEN
		Assert.assertTrue(result);
	}

}
