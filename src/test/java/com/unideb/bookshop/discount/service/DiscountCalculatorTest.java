package com.unideb.bookshop.discount.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DiscountCalculatorTest {

	private DiscountCalculator underTest;

	@Before
	public void setUp() {
		underTest = new DiscountCalculator();
	}

	@Test
	public void testGetDiscount() {
		// GIVEN
		double price = 10;
		double discount = 10;
		double expected = 9;

		// WHEN
		double actual = underTest.getDiscount(price, discount);

		// THEN
		Assert.assertEquals(expected, actual, 0);
	}

}
