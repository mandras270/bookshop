package com.unideb.bookshop.book.service;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.unideb.bookshop.book.domain.Book;
import com.unideb.bookshop.book.repository.dao.BookDao;
import com.unideb.bookshop.book.repository.domain.BookEntity;
import com.unideb.bookshop.book.service.transform.BookTransformer;

public class BookSearchServiceTest {

	@Mock
	private BookDao bookDao;

	@Mock
	private BookTransformer transformer;

	@InjectMocks
	private BookSearchService underTest;

	@Before
	public void setUp() {
		underTest = new BookSearchService();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testListBooks() {
		// GIVEN
		BookEntity entityMock = Mockito.mock(BookEntity.class);
		List<BookEntity> bookEntitys = Arrays.asList(entityMock);
		Mockito.when(bookDao.findByTitleIgnoreCaseLike(Mockito.anyString())).thenReturn(bookEntitys);
		Book bookMock = Mockito.mock(Book.class);
		List<Book> expected = Arrays.asList(bookMock);
		Mockito.when(transformer.transformBookEntities(bookEntitys)).thenReturn(expected);

		// WHEN
		List<Book> actual = underTest.listBooks();

		// THEN
		Assert.assertTrue(Arrays.deepEquals(expected.toArray(), actual.toArray()));
	}

	@Test
	public void testFindOnebyId() {
		// GIVEN
		long id = 1;
		BookEntity entityMock = Mockito.mock(BookEntity.class);
		Mockito.when(bookDao.findOne(id)).thenReturn(entityMock);
		Book expected = Mockito.mock(Book.class);
		Mockito.when(transformer.transformBookEntity(entityMock)).thenReturn(expected);

		// WHEN
		Book actual = underTest.findOnebyId(id);

		// THEN
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testListBooksByTitle() {
		// GIVEN
		BookEntity entityMock = Mockito.mock(BookEntity.class);
		List<BookEntity> bookEntitys = Arrays.asList(entityMock);
		String title = "bookTitle";
		Mockito.when(bookDao.findByTitleIgnoreCaseLike(String.format("%%%s%%", title))).thenReturn(bookEntitys);
		Book bookMock = Mockito.mock(Book.class);
		List<Book> expected = Arrays.asList(bookMock);
		Mockito.when(transformer.transformBookEntities(bookEntitys)).thenReturn(expected);

		// WHEN
		List<Book> actual = underTest.listBooksByTitle(title);

		// THEN
		Assert.assertTrue(Arrays.deepEquals(expected.toArray(), actual.toArray()));
	}
}
