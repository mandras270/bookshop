package com.unideb.bookshop.book.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.unideb.bookshop.book.domain.Book;
import com.unideb.bookshop.book.repository.dao.BookDao;
import com.unideb.bookshop.book.repository.domain.BookEntity;
import com.unideb.bookshop.book.service.transform.BookTransformer;

public class BookWriteServiceTest {

	@Mock
	private BookDao bookDao;

	@Mock
	private BookTransformer bookTransformer;

	@InjectMocks
	private BookWriteService underTest;

	@Before
	public void setUp() {
		underTest = new BookWriteService();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testSaveBook() {
		// GIVEN
		Book bookMock = Mockito.mock(Book.class);
		BookEntity bookEntityMock = Mockito.mock(BookEntity.class);
		Mockito.when(bookTransformer.transformBook(bookMock)).thenReturn(bookEntityMock);
		Mockito.when(bookDao.save(bookEntityMock)).thenReturn(bookEntityMock);
		long expected = 1;
		Mockito.when(bookEntityMock.getId()).thenReturn(expected);

		// WHEN
		long actual = underTest.saveBook(bookMock);

		// THEN
		Assert.assertEquals(expected, actual);
	}

}
