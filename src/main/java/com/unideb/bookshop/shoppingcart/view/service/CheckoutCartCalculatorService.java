package com.unideb.bookshop.shoppingcart.view.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unideb.bookshop.book.domain.Book;
import com.unideb.bookshop.book.service.BookSearchService;
import com.unideb.bookshop.browse.view.model.BookView;
import com.unideb.bookshop.discount.domain.BookDiscount;
import com.unideb.bookshop.discount.service.DiscountCalculator;
import com.unideb.bookshop.discount.service.DiscountSearchService;
import com.unideb.bookshop.shoppingcart.view.model.CheckoutCart;
import com.unideb.bookshop.shoppingcart.view.model.CheckoutCartItem;
import com.unideb.bookshop.shoppingcart.view.model.ShoppingCartItem;

@Service
public class CheckoutCartCalculatorService {

	@Autowired
	private BookSearchService bookSearchService;

	@Autowired
	private DiscountSearchService discountSearchService;

	@Autowired
	private DiscountCalculator discountCalculator;

	public CheckoutCart createCheckoutCart(List<ShoppingCartItem> items) {

		CheckoutCart checkoutCart = new CheckoutCart();

		List<CheckoutCartItem> checkoutCartItems = new ArrayList<>();

		double checkoutTotalPrice = 0;

		for (ShoppingCartItem item : items) {

			BookView book = item.getBook();
			long bookId = book.getBookId();

			Book originalBook = bookSearchService.findOnebyId(bookId);

			CheckoutCartItem checkoutCartItem = new CheckoutCartItem();
			checkoutCartItem.setBookId(originalBook.getId());
			checkoutCartItem.setTitle(originalBook.getTitle());
			checkoutCartItem.setAuthor(originalBook.getAuthor());
			checkoutCartItem.setOriginalPrice(originalBook.getPrice());
			checkoutCartItem.setQuantity(item.getQuantity());

			BookDiscount discount = discountSearchService.getBookDiscountByBookId(bookId);

			if (discount != null) {

				Double discountPercentage = discount.getDiscountPercentage();
				Double discountPrice = discountCalculator.getDiscount(originalBook.getPrice(), discountPercentage);
				checkoutCartItem.setSalePrice(discountPrice);

				Double totalPrice = discountPrice * item.getQuantity();
				checkoutCartItem.setTotalPrice(totalPrice);
				checkoutTotalPrice += totalPrice;

			} else {

				Double totalPrice = originalBook.getPrice() * item.getQuantity();
				checkoutTotalPrice += totalPrice;
				checkoutCartItem.setTotalPrice(totalPrice);

			}

			checkoutCartItems.add(checkoutCartItem);
		}
		checkoutCart.setTotalPrice(checkoutTotalPrice);
		checkoutCart.setItems(checkoutCartItems);
		return checkoutCart;
	}

}
