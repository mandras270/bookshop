package com.unideb.bookshop.shoppingcart.view.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShoppingCart {

    public static final String SHOPPING_CART_KEY = "SHOPPING_CART";
    private Map<Long, ShoppingCartItem> items;
    private int length;

    public ShoppingCart() {
        super();
        this.items = new HashMap<>();
    }

    public List<ShoppingCartItem> getItems() {
        return new ArrayList<ShoppingCartItem>(items.values());
    }

    public void clear() {
        items.clear();
    }

    public void addBook(ShoppingCartItem item) {

        ShoppingCartItem shoppingCartItem = items.get(item.getBookId());
        Integer quantity;
        if (shoppingCartItem == null) {
            quantity = item.getQuantity();
        } else {
            quantity = shoppingCartItem.getQuantity() + item.getQuantity();
        }
        items.put(item.getBookId(), new ShoppingCartItem(item.getBook(), quantity));

        ++length;

    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

}
