package com.unideb.bookshop.shoppingcart.view.service.transform;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unideb.bookshop.book.domain.Book;
import com.unideb.bookshop.browse.view.model.BookView;
import com.unideb.bookshop.browse.view.transform.BookModelTransformer;
import com.unideb.bookshop.shoppingcart.view.model.ShoppingCartItem;

@Component
public class ShoppingCartItemTransformer {

	private static final int DEFAULT_QUANTITY = 1;
	@Autowired
	private BookModelTransformer bookModelTransformer;

	public ShoppingCartItem transformBookToShoppingCartItem(Book book, double discountPercentage) {

		BookView bookView = bookModelTransformer.transformBookView(book, discountPercentage);

		return new ShoppingCartItem(bookView, DEFAULT_QUANTITY);
	}

}
