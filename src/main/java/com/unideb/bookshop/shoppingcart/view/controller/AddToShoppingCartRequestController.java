package com.unideb.bookshop.shoppingcart.view.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.unideb.bookshop.book.domain.Book;
import com.unideb.bookshop.book.service.BookSearchService;
import com.unideb.bookshop.browse.view.controller.ListBooksController;
import com.unideb.bookshop.discount.domain.BookDiscount;
import com.unideb.bookshop.discount.service.DiscountSearchService;
import com.unideb.bookshop.shoppingcart.view.model.AddToCartRequest;
import com.unideb.bookshop.shoppingcart.view.model.ShoppingCart;
import com.unideb.bookshop.shoppingcart.view.model.ShoppingCartItem;
import com.unideb.bookshop.shoppingcart.view.service.transform.ShoppingCartItemTransformer;

@Controller
@SessionAttributes(value = ShoppingCart.SHOPPING_CART_KEY)
public class AddToShoppingCartRequestController {

	private static final String MESSAGE = "message";

	private static final String SUCCESS = "success";

	public static final String REQUEST_MAPPING = "/addToCart";

	@Autowired
	private BookSearchService bookSearchService;

	@Autowired
	private DiscountSearchService discountSearchService;

	@Autowired
	private ShoppingCartItemTransformer shoppingCartItemTransformer;

	@ModelAttribute("addToCartRequest")
	public AddToCartRequest addToCartRequest() {
		return new AddToCartRequest();
	}

	@ModelAttribute("shoppingCart")
	public ShoppingCart shoppingCart(@ModelAttribute(ShoppingCart.SHOPPING_CART_KEY) ShoppingCart shoppingCart) {
		ShoppingCart result;
		if (shoppingCart == null) {
			result = new ShoppingCart();
		} else {
			result = shoppingCart;
		}
		return result;
	}

	@RequestMapping(value = REQUEST_MAPPING, method = RequestMethod.POST)
	public String addToCart(AddToCartRequest addToCartRequest, ShoppingCart shoppingCart,
			RedirectAttributes redirectAttributes) {

		Book book = bookSearchService.findOnebyId(addToCartRequest.getId());
		BookDiscount bookDiscount = discountSearchService.getBookDiscountByBookId(book.getId());

		if (bookDiscount != null) {
			Double discountPercentage = bookDiscount.getDiscountPercentage();
			ShoppingCartItem item = shoppingCartItemTransformer.transformBookToShoppingCartItem(book,
					discountPercentage);
			shoppingCart.addBook(item);
		} else {
			ShoppingCartItem item = shoppingCartItemTransformer.transformBookToShoppingCartItem(book, 0);
			shoppingCart.addBook(item);
		}

		redirectAttributes.addFlashAttribute(MESSAGE, SUCCESS);

		return String.format("redirect:%s", ListBooksController.REQUEST_MAPPING);
	}
}
