package com.unideb.bookshop.shoppingcart.view.model;


public class AddToCartRequest {

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
