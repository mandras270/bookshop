package com.unideb.bookshop.shoppingcart.view.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.unideb.bookshop.authentication.service.AuthenticationService;
import com.unideb.bookshop.regularcustomer.domain.RegularCustomerToken;
import com.unideb.bookshop.regularcustomer.service.RegularCustomerPriceCalculator;
import com.unideb.bookshop.regularcustomer.service.RegularCustomerTokenSearchService;
import com.unideb.bookshop.shoppingcart.view.model.CheckoutCart;
import com.unideb.bookshop.shoppingcart.view.model.ShoppingCart;
import com.unideb.bookshop.shoppingcart.view.model.ShoppingCartItem;
import com.unideb.bookshop.shoppingcart.view.service.CheckoutCartCalculatorService;
import com.unideb.bookshop.user.service.UserSearchService;

@Controller
@SessionAttributes(value = ShoppingCart.SHOPPING_CART_KEY)
public class ViewShoppingCartController {

	private static final String REQUEST_MAPPING = "/checkout";

	@Autowired
	private UserSearchService userSearchService;

	@Autowired
	private CheckoutCartCalculatorService checkoutCartCalculatorService;

	@Autowired
	private RegularCustomerPriceCalculator regularCustomerPriceCalculator;

	@Autowired
	private RegularCustomerTokenSearchService tokenSearchService;

	@Autowired
	private AuthenticationService authenticationService;

	/**
	 * 
	 * Returns a {@link CheckoutCart} based on the {@link ShoppingCart} content
	 * or null if the ShoppingCart model does not exist or has an empty value.
	 * 
	 * @param shoppingCart
	 *            The ShoppingCart model
	 * @return A CheckoutCart model or null
	 */
	@ModelAttribute("shoppingCartContent")
	public CheckoutCart shoppingCartContent(@ModelAttribute(ShoppingCart.SHOPPING_CART_KEY) ShoppingCart shoppingCart) {

		CheckoutCart result = null;

		if (shoppingCart != null && shoppingCart.getItems().size() > 0) {
			List<ShoppingCartItem> items = shoppingCart.getItems();

			result = checkoutCartCalculatorService.createCheckoutCart(items);

			long userId = userSearchService.getUserIdByUsernameOrEmail(authenticationService.getUserName());

			RegularCustomerToken token = tokenSearchService.findRegularCustomerTokenForUser(userId);

			if (token != null) {
				regularCustomerPriceCalculator.appyRegularCustomerPricees(result);
			}
		}

		return result;
	}

	@RequestMapping(REQUEST_MAPPING)
	public String viewShoppingCart() {
		return "shoppingCart";
	}

}
