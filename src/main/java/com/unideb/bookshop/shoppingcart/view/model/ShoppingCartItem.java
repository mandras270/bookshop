package com.unideb.bookshop.shoppingcart.view.model;

import com.unideb.bookshop.browse.view.model.BookView;

public class ShoppingCartItem {

    private BookView book;
    private Integer quantity;

    public ShoppingCartItem(BookView book, Integer quantity) {
        super();
        this.book = book;
        this.quantity = quantity;
    }

    public BookView getBook() {
        return book;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public Long getBookId() {
        return book.getBookId();
    }

}
