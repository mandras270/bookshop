package com.unideb.bookshop.shoppingcart.view.model;

import java.util.List;

import com.unideb.bookshop.regularcustomer.domain.RegularCustomerToken;

public class CheckoutCart {

	private List<CheckoutCartItem> items;
	private double totalPrice;
	private Double regularCustomerPrice;

	public List<CheckoutCartItem> getItems() {
		return items;
	}

	public void setItems(List<CheckoutCartItem> items) {
		this.items = items;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * Returns the price for a user with a {@link RegularCustomerToken} or null
	 * if the user does not have one.
	 * 
	 * @return The price for a RegularCustomer or null
	 */
	public Double getRegularCustomerPrice() {
		return regularCustomerPrice;
	}

	public void setRegularCustomerPrice(Double regularCustomerPrice) {
		this.regularCustomerPrice = regularCustomerPrice;
	}
}
