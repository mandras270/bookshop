package com.unideb.bookshop.registration.view.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.unideb.bookshop.registration.view.domain.RegistrationRequest;
import com.unideb.bookshop.registration.view.service.transform.RegistrationRequestTransformer;
import com.unideb.bookshop.user.service.UserWriteService;

@Controller
public class AddUserPostController {

	private static final String FAIL_MSG = "Unable to save this record. Username or Email exists!";

	public static final String REQUEST_MAPPING = "/saveUser";

	@Autowired
	private UserWriteService userWriteService;

	@Autowired
	private RegistrationRequestTransformer transformer;

	@RequestMapping(value = REQUEST_MAPPING, method = RequestMethod.POST)
	public String saveUser(@Valid RegistrationRequest registrationRequest, BindingResult bindingResult,
			RedirectAttributes redirectAttributes) {

		String result;
		if (bindingResult.hasErrors()) {
			result = "registration";
		} else {

			try {
				userWriteService.save(transformer.transformRegistrationRequest(registrationRequest));
				String successMessage = String.format("%s saved!", registrationRequest.getEmail());
				redirectAttributes.addFlashAttribute("success", true);
				redirectAttributes.addFlashAttribute("message", successMessage);
				result = "redirect:/registration";
			} catch (Exception e) {
				redirectAttributes.addFlashAttribute("success", false);
				redirectAttributes.addFlashAttribute("message", FAIL_MSG);
				result = "redirect:/registration";
			}

		}
		return result;
	}

}
