package com.unideb.bookshop.registration.view.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.unideb.bookshop.registration.view.domain.RegistrationRequest;

@Controller
public class AddUserFormController {

	public static final String REQUEST_MAPPING = "/registration";

	@ModelAttribute("registrationRequest")
	public RegistrationRequest registrationRequest() {
		return new RegistrationRequest();
	}

	@RequestMapping(REQUEST_MAPPING)
	public String registrationPage() {
		return "registration";
	}

}
