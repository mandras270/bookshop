package com.unideb.bookshop.registration.view.service.transform;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.unideb.bookshop.registration.view.domain.RegistrationRequest;
import com.unideb.bookshop.user.domain.User;
import com.unideb.bookshop.user.domain.UserRole;

@Service
public class RegistrationRequestTransformer {

	public User transformRegistrationRequest(RegistrationRequest registrationRequest) {

		User result = new User();

		Set<UserRole> userRoles = new HashSet<UserRole>(Arrays.asList(UserRole.ROLE_USER));

		result.setEmail(registrationRequest.getEmail());
		result.setEnabled(true);
		result.setPassword(registrationRequest.getPassword());
		result.setUsername(registrationRequest.getUsername());
		result.setUserRoles(userRoles);

		return result;
	}

}
