package com.unideb.bookshop.search.view.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.unideb.bookshop.search.view.model.SearchModel;

@ControllerAdvice
public class SearchController {

    @ModelAttribute("searchModel")
    public SearchModel searchModel() {
        return new SearchModel();
    }
}
