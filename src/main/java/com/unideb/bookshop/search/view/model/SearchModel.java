package com.unideb.bookshop.search.view.model;

public class SearchModel {

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
