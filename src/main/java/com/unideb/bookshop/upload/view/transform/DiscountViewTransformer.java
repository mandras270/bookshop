package com.unideb.bookshop.upload.view.transform;

import org.springframework.stereotype.Component;

import com.unideb.bookshop.discount.domain.BookDiscount;

@Component
public class DiscountViewTransformer {

	public BookDiscount createBookDiscount(Double discountPercentage) {

		BookDiscount result = null;

		if (discountPercentage != null && discountPercentage != 0) {
			result = new BookDiscount();
			result.setDiscountPercentage(discountPercentage);
		}

		return result;
	}
}
