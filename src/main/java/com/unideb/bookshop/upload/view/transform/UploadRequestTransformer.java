package com.unideb.bookshop.upload.view.transform;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.unideb.bookshop.book.domain.Book;
import com.unideb.bookshop.upload.view.model.UploadRequest;

@Component
public class UploadRequestTransformer {

	private static final String NO_COVER_FOUND = "No cover found...";
	private static final String COVER_FOUND = "The cover was not null, setting cover..";
	private static final String COVER_EX = "Failed to get cover";
	private static Logger LOGGER = LoggerFactory.getLogger(UploadRequestTransformer.class);

	public Book transformUploadRequestToBook(UploadRequest uploadRequest) {

		Book result = new Book();

		result.setTitle(uploadRequest.getTitle());
		result.setAuthor(uploadRequest.getAuthor());
		result.setSynopsis(uploadRequest.getSynopsis());
		result.setPrice(uploadRequest.getPrice());
		setCover(uploadRequest, result);

		return result;
	}

	private void setCover(UploadRequest uploadRequest, Book result) {
		try {
			MultipartFile cover = uploadRequest.getCover();
			if (cover != null && !cover.isEmpty()) {
				result.setCover(cover.getBytes());
				LOGGER.info(COVER_FOUND);
			} else {
				result.setCover(null);
				LOGGER.info(NO_COVER_FOUND);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.info(COVER_EX);
		}
	}

}
