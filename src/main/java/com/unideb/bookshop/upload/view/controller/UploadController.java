package com.unideb.bookshop.upload.view.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.unideb.bookshop.upload.view.model.UploadRequest;

@Controller
public class UploadController {

    public static final String REQUEST_MAPPING = "/upload";

    @ModelAttribute("uploadRequest")
    public UploadRequest uploadRequest() {
        return new UploadRequest();
    }

    @RequestMapping(REQUEST_MAPPING)
    public String upload() {
        return "upload";
    }

}
