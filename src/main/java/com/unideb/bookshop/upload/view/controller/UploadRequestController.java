package com.unideb.bookshop.upload.view.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.unideb.bookshop.book.domain.Book;
import com.unideb.bookshop.book.service.BookWriteService;
import com.unideb.bookshop.discount.domain.BookDiscount;
import com.unideb.bookshop.discount.service.DiscountWriteService;
import com.unideb.bookshop.upload.view.model.UploadRequest;
import com.unideb.bookshop.upload.view.transform.DiscountViewTransformer;
import com.unideb.bookshop.upload.view.transform.UploadRequestTransformer;

@Controller
public class UploadRequestController {

	private static final String SUCCESS = "success";

	private static final String MESSAGE = "message";

	private static final String REQUEST_MAPPING = "/uploadBook";

	@Autowired
	private BookWriteService bookWriteService;

	@Autowired
	private DiscountWriteService discountWriteService;

	@Autowired
	private UploadRequestTransformer uploadRequestTransformer;

	@Autowired
	private DiscountViewTransformer discountViewTransformer;

	@RequestMapping(value = REQUEST_MAPPING, method = RequestMethod.POST)
	public String upload(@Valid UploadRequest uploadRequest, BindingResult bindingResult,
			RedirectAttributes redirectAttributes) {

		String result = "upload";

		if (!bindingResult.hasErrors()) {

			Book book = uploadRequestTransformer.transformUploadRequestToBook(uploadRequest);
			Double discountPercentage = uploadRequest.getDiscountPercentage();
			BookDiscount bookDiscount = discountViewTransformer.createBookDiscount(discountPercentage);

			try {
				long bookId = bookWriteService.saveBook(book);

				if (bookDiscount != null) {
					bookDiscount.setBookId(bookId);
					discountWriteService.save(bookDiscount);
				}

				redirectAttributes.addFlashAttribute(MESSAGE, SUCCESS);
			} catch (Exception e) {
				e.printStackTrace();
				String message = String.format("%s: %s", e, e.getMessage());
				redirectAttributes.addFlashAttribute(MESSAGE, message);
			}

		}

		result = String.format("redirect:%s", UploadController.REQUEST_MAPPING);
		return result;
	}
}
