package com.unideb.bookshop.login.view.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.unideb.bookshop.home.view.model.LoginRequest;

@Controller
public class LoginController {

	public static final String REQUEST_MAPPING = "/login";

	@ModelAttribute("loginRequest")
	public LoginRequest createLoginRequest() {
		return new LoginRequest();
	}

	@RequestMapping(REQUEST_MAPPING)
	public String loginPage() {
		return "login";
	}

}
