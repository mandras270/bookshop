package com.unideb.bookshop.regularcustomer.service.transform;

import org.springframework.stereotype.Component;

import com.unideb.bookshop.regularcustomer.domain.RegularCustomerToken;
import com.unideb.bookshop.regularcustomer.repository.domain.RegularCustomerTokenEntity;

@Component
public class RegularCustomerTokenTransformer {

	public RegularCustomerToken transformRegularCustomerTokenEntity(RegularCustomerTokenEntity entity) {

		RegularCustomerToken result = null;

		if (entity != null) {
			result = new RegularCustomerToken();
			result.setUserId(entity.getUserId());
		}

		return result;
	}

}
