package com.unideb.bookshop.regularcustomer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unideb.bookshop.regularcustomer.domain.RegularCustomerToken;
import com.unideb.bookshop.regularcustomer.repository.dao.RegularCustomerTokenDao;
import com.unideb.bookshop.regularcustomer.repository.domain.RegularCustomerTokenEntity;
import com.unideb.bookshop.regularcustomer.service.transform.RegularCustomerTokenTransformer;

@Service
public class RegularCustomerTokenSearchService {

	@Autowired
	private RegularCustomerTokenDao customerTokenDao;

	@Autowired
	private RegularCustomerTokenTransformer customerTokenTransformer;

	public RegularCustomerToken findRegularCustomerTokenForUser(long userId) {
		RegularCustomerTokenEntity entity = customerTokenDao.findOneByUserId(userId);
		return customerTokenTransformer.transformRegularCustomerTokenEntity(entity);
	}
}
