package com.unideb.bookshop.regularcustomer.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.unideb.bookshop.shoppingcart.view.model.CheckoutCart;
import com.unideb.bookshop.shoppingcart.view.model.CheckoutCartItem;

@Component
public class RegularCustomerPriceCalculator {

	private static final int REGULAR_CUSTOMER_ALLOTMENT = 10;

	private static Logger LOGGER = LoggerFactory.getLogger(RegularCustomerPriceCalculator.class);

	public void appyRegularCustomerPricees(final CheckoutCart checkoutCart) {

		List<CheckoutCartItem> items = checkoutCart.getItems();

		double regularCustomerPrice = 0;

		for (CheckoutCartItem item : items) {

			double totalPrice = item.getTotalPrice();

			LOGGER.info("Item total price: " + totalPrice);

			double discountPrice = totalPrice * (100.0 - REGULAR_CUSTOMER_ALLOTMENT) / 100;

			LOGGER.info("Item total price with regular customer discount: " + discountPrice);

			regularCustomerPrice += discountPrice;

		}

		checkoutCart.setRegularCustomerPrice(regularCustomerPrice);
	}

}
