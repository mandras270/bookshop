package com.unideb.bookshop.regularcustomer.repository.dao;

import org.springframework.data.repository.CrudRepository;

import com.unideb.bookshop.regularcustomer.repository.domain.RegularCustomerTokenEntity;

public interface RegularCustomerTokenDao extends CrudRepository<RegularCustomerTokenEntity, Long> {

	public RegularCustomerTokenEntity findOneByUserId(long userId);

}
