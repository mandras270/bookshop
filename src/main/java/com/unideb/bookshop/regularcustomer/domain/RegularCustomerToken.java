package com.unideb.bookshop.regularcustomer.domain;

public class RegularCustomerToken {

	private long userId;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

}
