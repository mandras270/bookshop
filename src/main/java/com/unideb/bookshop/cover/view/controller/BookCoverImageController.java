package com.unideb.bookshop.cover.view.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.AbstractResource;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unideb.bookshop.book.service.BookSearchService;

@Controller
public class BookCoverImageController {
    private static final String COVER_FOUND = "Cover found which length is %d bytes";

    public static final String REQUEST_MAPPING = "/bookCover";

    private static Logger LOGGER = LoggerFactory.getLogger(BookCoverImageController.class);

    @Autowired
    private BookSearchService bookSearchService;

    @RequestMapping(value = REQUEST_MAPPING, method = RequestMethod.GET)
    @ResponseBody
    public AbstractResource downloadCover(@RequestParam(value = "bookId") long id) {

        ByteArrayResource result = null;

        byte[] cover = bookSearchService.findOnebyId(id).getCover();

        if (cover != null) {
            result = new ByteArrayResource(cover);

            String message = String.format(COVER_FOUND, cover.length);
            LOGGER.info(message);
        }

        return result;
    }
}