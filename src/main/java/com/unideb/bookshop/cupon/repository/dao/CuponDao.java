package com.unideb.bookshop.cupon.repository.dao;

import org.springframework.data.repository.CrudRepository;

import com.unideb.bookshop.cupon.repository.domain.CuponEntity;

public interface CuponDao extends CrudRepository<CuponEntity, Long> {

	public CuponEntity findOneByCode(String code);
}
