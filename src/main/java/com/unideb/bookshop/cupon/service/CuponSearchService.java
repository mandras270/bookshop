package com.unideb.bookshop.cupon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unideb.bookshop.cupon.domain.Cupon;
import com.unideb.bookshop.cupon.repository.dao.CuponDao;
import com.unideb.bookshop.cupon.repository.domain.CuponEntity;
import com.unideb.bookshop.cupon.service.transform.CuponTransformer;

@Service
public class CuponSearchService {

	@Autowired
	private CuponDao cuponDao;

	private CuponTransformer cuponTransformer;

	/**
	 * Searches the database for the cupon code. If the code is not found, null
	 * value will be returned.
	 * 
	 * @param code
	 *            The cupon code to search for
	 * @return The cupon object if found or null
	 */
	public Cupon findCuponByCode(String code) {
		CuponEntity entity = cuponDao.findOneByCode(code);
		return cuponTransformer.transformCuponEntity(entity);
	}
}
