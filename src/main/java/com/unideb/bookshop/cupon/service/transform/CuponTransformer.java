package com.unideb.bookshop.cupon.service.transform;

import org.springframework.stereotype.Component;

import com.unideb.bookshop.cupon.domain.Cupon;
import com.unideb.bookshop.cupon.repository.domain.CuponEntity;

@Component
public class CuponTransformer {

	public Cupon transformCuponEntity(CuponEntity entity) {

		Cupon result = null;

		if (entity != null) {

			result = new Cupon();
			result.setCode(entity.getCode());
			result.setDiscountPercentage(entity.getDiscountPercentage());

		}

		return result;

	}
}
