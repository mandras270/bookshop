package com.unideb.bookshop.authentication.view.model;

public class UserAuthenticationModel {

	private boolean admin;
	private boolean userAuthenticated;

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public boolean isUserAuthenticated() {
		return userAuthenticated;
	}

	public void setUserAuthenticated(boolean userAuthenticated) {
		this.userAuthenticated = userAuthenticated;
	}
}
