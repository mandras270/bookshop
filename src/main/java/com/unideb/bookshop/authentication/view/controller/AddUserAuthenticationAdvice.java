package com.unideb.bookshop.authentication.view.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.unideb.bookshop.authentication.service.AuthenticationService;
import com.unideb.bookshop.authentication.view.model.UserAuthenticationModel;

@ControllerAdvice
public class AddUserAuthenticationAdvice {

	@Autowired
	private AuthenticationService authenticationService;

	@ModelAttribute("authentication")
	public UserAuthenticationModel userAuthentication() {

		UserAuthenticationModel authentication = new UserAuthenticationModel();
		authentication.setAdmin(authenticationService.isUserAdmin());
		authentication.setUserAuthenticated(authenticationService.isUserAuthenticated());

		return authentication;
	}
}
