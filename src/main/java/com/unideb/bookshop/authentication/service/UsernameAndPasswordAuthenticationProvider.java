package com.unideb.bookshop.authentication.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.unideb.bookshop.user.domain.User;
import com.unideb.bookshop.user.domain.UserRole;
import com.unideb.bookshop.user.service.UserSearchService;

@Component
public class UsernameAndPasswordAuthenticationProvider implements AuthenticationProvider {

	private static final String ROLE_GRANTED_MSG = "Role %s granted.";
	private static Logger LOGGER = LoggerFactory.getLogger(UsernameAndPasswordAuthenticationProvider.class);
	private static final String LOGIN_FAILED_MESSAGE = "Bad credentials";
	@Autowired
	private UserSearchService userSearchService;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		Authentication result = null;

		String username = authentication.getName();
		String password = authentication.getCredentials().toString();

		if (!username.contains("@")) {

			User user = userSearchService.findOneByUsernameAndPassword(username, password);

			if (user != null) {

				List<GrantedAuthority> grantedAuths = new ArrayList<>();

				for (UserRole role : user.getUserRoles()) {
					String roleString = role.toString();
					grantedAuths.add(new SimpleGrantedAuthority(roleString));
					LOGGER.info(String.format(ROLE_GRANTED_MSG, roleString));
				}

				result = new UsernamePasswordAuthenticationToken(username, password, grantedAuths);

			} else {
				throw new BadCredentialsException(LOGIN_FAILED_MESSAGE);
			}
		}
		return result;

	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
