package com.unideb.bookshop.authentication.service;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.unideb.bookshop.user.domain.UserRole;

@Service
public class AuthenticationService {

	public boolean isUserAuthenticated() {
		return checkIfUserRoleExists(UserRole.ROLE_USER);
	}

	public boolean isUserAdmin() {
		boolean result = false;
		if (isUserAuthenticated()) {
			result = checkAdmin();
		}
		return result;
	}

	public String getUserName() {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		Authentication authentication = securityContext.getAuthentication();
		return authentication.getName();
	}

	private boolean checkAdmin() {
		return checkIfUserRoleExists(UserRole.ROLE_ADMIN);
	}

	private boolean checkIfUserRoleExists(UserRole role) {
		String roleAsString = role.toString();
		boolean result = false;
		SecurityContext securityContext = SecurityContextHolder.getContext();
		Authentication authentication = securityContext.getAuthentication();
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

		for (GrantedAuthority grantedAuthority : authorities) {
			if (roleAsString.equals(grantedAuthority.toString())) {
				result = true;
				break;
			}
		}

		return result;
	}
}