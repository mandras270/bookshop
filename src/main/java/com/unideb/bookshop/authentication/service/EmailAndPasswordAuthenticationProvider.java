package com.unideb.bookshop.authentication.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.unideb.bookshop.user.domain.User;
import com.unideb.bookshop.user.domain.UserRole;
import com.unideb.bookshop.user.service.UserSearchService;

@Component
public class EmailAndPasswordAuthenticationProvider implements AuthenticationProvider {

	private static final String LOGIN_FAILED_MESSAGE = "Bad credentials";
	@Autowired
	private UserSearchService userSearchService;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		Authentication result = null;

		String username = authentication.getName();
		String password = authentication.getCredentials().toString();

		if (username.contains("@")) {

			User user = userSearchService.findOneByEmailAndPassword(username, password);

			if (user != null) {

				List<GrantedAuthority> grantedAuths = new ArrayList<>();

				for (UserRole role : user.getUserRoles()) {
					grantedAuths.add(new SimpleGrantedAuthority(role.toString()));
				}

				result = new UsernamePasswordAuthenticationToken(username, password, grantedAuths);

			} else {
				throw new BadCredentialsException(LOGIN_FAILED_MESSAGE);
			}
		}
		return result;

	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
