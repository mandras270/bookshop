package com.unideb.bookshop.book.repository.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.unideb.bookshop.book.repository.domain.BookEntity;

public interface BookDao extends CrudRepository<BookEntity, Long> {

    public List<BookEntity> findByTitleIgnoreCaseLike(String title);
}
