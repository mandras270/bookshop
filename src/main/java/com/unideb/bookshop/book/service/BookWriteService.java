package com.unideb.bookshop.book.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unideb.bookshop.book.domain.Book;
import com.unideb.bookshop.book.repository.dao.BookDao;
import com.unideb.bookshop.book.repository.domain.BookEntity;
import com.unideb.bookshop.book.service.transform.BookTransformer;

@Service
public class BookWriteService {

	@Autowired
	private BookDao bookDao;

	@Autowired
	private BookTransformer bookTransformer;

	public long saveBook(Book book) {

		BookEntity bookEntity = bookTransformer.transformBook(book);

		BookEntity savedBook = bookDao.save(bookEntity);

		return savedBook.getId();
	}

}
