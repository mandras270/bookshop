package com.unideb.bookshop.book.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unideb.bookshop.book.domain.Book;
import com.unideb.bookshop.book.repository.dao.BookDao;
import com.unideb.bookshop.book.repository.domain.BookEntity;
import com.unideb.bookshop.book.service.transform.BookTransformer;

@Service
public class BookSearchService {

    @Autowired
    private BookDao bookDao;

    @Autowired
    private BookTransformer transformer;

    public List<Book> listBooks() {
        List<BookEntity> entities = findBookEntities(formatQuery(null));
        List<Book> result = transformer.transformBookEntities(entities);
        return result;
    }

    public Book findOnebyId(long id) {
        return transformer.transformBookEntity(bookDao.findOne(id));
    }

    public List<Book> listBooksByTitle(String title) {
        List<BookEntity> entities = findBookEntities(formatQuery(title));
        List<Book> result = transformer.transformBookEntities(entities);
        return result;
    }

    private String formatQuery(String title) {
        String result;
        if (title == null) {
            result = "%";
        } else {
            result = String.format("%%%s%%", title);
        }
        return result;
    }

    private List<BookEntity> findBookEntities(String title) {
        return bookDao.findByTitleIgnoreCaseLike(title);
    }
}
