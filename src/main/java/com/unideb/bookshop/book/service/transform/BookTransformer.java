package com.unideb.bookshop.book.service.transform;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.unideb.bookshop.book.domain.Book;
import com.unideb.bookshop.book.repository.domain.BookEntity;

@Component
public class BookTransformer {

	public BookEntity transformBook(Book book) {

		BookEntity result = new BookEntity();

		result.setAuthor(book.getAuthor());
		result.setCover(book.getCover());
		result.setId(book.getId());
		result.setSynopsis(book.getSynopsis());
		result.setTitle(book.getTitle());
		result.setPrice(book.getPrice());

		return result;
	}

	public Book transformBookEntity(BookEntity entity) {

		Book result = new Book();

		result.setAuthor(entity.getAuthor());
		result.setCover(entity.getCover());
		result.setId(entity.getId());
		result.setSynopsis(entity.getSynopsis());
		result.setTitle(entity.getTitle());
		result.setPrice(entity.getPrice());

		return result;
	}

	public List<Book> transformBookEntities(List<BookEntity> entities) {

		List<Book> result = new ArrayList<>();

		for (BookEntity entity : entities) {
			Book book = transformBookEntity(entity);
			result.add(book);
		}

		return result;
	}

}
