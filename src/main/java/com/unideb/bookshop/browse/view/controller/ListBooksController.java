package com.unideb.bookshop.browse.view.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.unideb.bookshop.book.domain.Book;
import com.unideb.bookshop.book.service.BookSearchService;
import com.unideb.bookshop.browse.view.model.ListBookModel;
import com.unideb.bookshop.browse.view.transform.BookModelTransformer;
import com.unideb.bookshop.discount.domain.BookDiscount;
import com.unideb.bookshop.discount.service.DiscountSearchService;
import com.unideb.bookshop.search.view.model.SearchModel;
import com.unideb.bookshop.shoppingcart.view.model.AddToCartRequest;

@Controller
public class ListBooksController {

	public static final String REQUEST_MAPPING = "/browse";

	@Autowired
	private BookSearchService bookSearchService;

	@Autowired
	private DiscountSearchService discountSearchService;

	@Autowired
	private BookModelTransformer bookModelTransformer;

	@ModelAttribute("listBookModel")
	public ListBookModel createListBookModel(SearchModel searchModel) {
		List<Book> books = bookSearchService.listBooksByTitle(searchModel.getTitle());
		ListBookModel result = bookModelTransformer.transformBookList(createBookAndDiscountMap(books));
		return result;
	}

	private Map<Book, Double> createBookAndDiscountMap(List<Book> books) {

		Map<Book, Double> result = new HashMap<>();

		for (Book book : books) {
			Long bookId = book.getId();
			BookDiscount discount = discountSearchService.getBookDiscountByBookId(bookId);

			if (discount != null) {
				result.put(book, discount.getDiscountPercentage());
			} else {
				double zero = 0;
				result.put(book, zero); // TODO check
			}
		}

		return result;
	}

	@ModelAttribute("addToCartRequest")
	public AddToCartRequest addToCartRequest() {
		return new AddToCartRequest();
	}

	@RequestMapping(REQUEST_MAPPING)
	public String listBooks() {
		return "browse";
	}

}
