package com.unideb.bookshop.browse.view.model;

import java.util.List;

public class ListBookModel {

	private List<BookView> books;

	public List<BookView> getBooks() {
		return books;
	}

	public void setBooks(List<BookView> books) {
		this.books = books;
	}

}
