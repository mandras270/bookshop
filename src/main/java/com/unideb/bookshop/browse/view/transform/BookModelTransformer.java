package com.unideb.bookshop.browse.view.transform;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unideb.bookshop.book.domain.Book;
import com.unideb.bookshop.browse.view.model.BookView;
import com.unideb.bookshop.browse.view.model.ListBookModel;
import com.unideb.bookshop.discount.service.DiscountCalculator;

@Component
public class BookModelTransformer {

	@Autowired
	private DiscountCalculator discountCalculator;

	public BookView transformBookView(Book book, double discountPercentage) {

		BookView result = new BookView();

		result.setAuthor(book.getAuthor());
		result.setBookId(book.getId());
		result.setPrice(book.getPrice());
		result.setSynopsis(book.getSynopsis());
		result.setTitle(book.getTitle());
		result.setHasCover(book.getCover() == null ? false : true);

		Double salePrice = discountCalculator.getDiscount(book.getPrice(), discountPercentage);
		result.setSalePrice(salePrice);

		return result;
	}

	public ListBookModel transformBookList(Map<Book, Double> booksAndDiscountPercentages) {
		List<BookView> bookviews = new ArrayList<>();

		for (Map.Entry<Book, Double> entry : booksAndDiscountPercentages.entrySet()) {

			Book book = entry.getKey();
			Double discountPercentage = entry.getValue();

			BookView bookview = transformBookView(book, discountPercentage);
			bookviews.add(bookview);
		}

		ListBookModel result = new ListBookModel();
		result.setBooks(bookviews);
		return result;
	}

}
