package com.unideb.bookshop.user.repository.dao;

import org.springframework.data.repository.CrudRepository;

import com.unideb.bookshop.user.repository.domain.UserEntity;

public interface UserDao extends CrudRepository<UserEntity, Long> {

	public UserEntity findOneByUsernameAndPassword(String username, String password);

	public UserEntity findOneByEmailAndPassword(String email, String password);

	public UserEntity findOneByUsername(String username);

	public UserEntity findOneByEmail(String email);

}
