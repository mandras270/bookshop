package com.unideb.bookshop.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unideb.bookshop.user.domain.User;
import com.unideb.bookshop.user.repository.dao.UserDao;
import com.unideb.bookshop.user.repository.domain.UserEntity;
import com.unideb.bookshop.user.service.transform.UserEntitryTransformer;

@Service
public class UserWriteService {

	@Autowired
	private UserEntitryTransformer transformer;

	@Autowired
	private UserDao userDao;

	public void save(User user) {
		UserEntity entity = transformer.transformUser(user);
		userDao.save(entity);
	}
}
