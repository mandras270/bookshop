package com.unideb.bookshop.user.service.transform;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.unideb.bookshop.user.domain.User;
import com.unideb.bookshop.user.repository.domain.UserEntity;

@Component
public class UserEntitryTransformer {

	private static final String USER_FOUND_MSG = "User found: ";
	private static Logger LOGGER = LoggerFactory.getLogger(UserEntitryTransformer.class);

	public User transformUserEntity(UserEntity userEntity) {

		User result = null;

		if (userEntity != null) {

			LOGGER.info(USER_FOUND_MSG + userEntity.getUsername());

			result = new User();
			result.setEmail(userEntity.getEmail());
			result.setEnabled(userEntity.isEnabled());
			result.setId(userEntity.getId());
			result.setUsername(userEntity.getUsername());
			result.setUserRoles(userEntity.getUserRoles());
			result.setPassword(userEntity.getPassword());
		}

		return result;
	}

	public UserEntity transformUser(User user) {
		UserEntity result = new UserEntity();

		result.setEmail(user.getEmail());
		result.setEnabled(user.isEnabled());
		result.setPassword(user.getPassword());
		result.setUsername(user.getUsername());
		result.setUserRoles(user.getUserRoles());

		return result;
	}
}
