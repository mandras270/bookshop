package com.unideb.bookshop.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unideb.bookshop.user.domain.User;
import com.unideb.bookshop.user.repository.dao.UserDao;
import com.unideb.bookshop.user.repository.domain.UserEntity;
import com.unideb.bookshop.user.service.transform.UserEntitryTransformer;

@Service
public class UserSearchService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private UserEntitryTransformer transformer;

	public User findOneByUsernameAndPassword(String username, String password) {
		UserEntity userEntity = userDao.findOneByUsernameAndPassword(username, password);
		return transformer.transformUserEntity(userEntity);
	}

	public User findOneByEmailAndPassword(String email, String password) {
		UserEntity userEntity = userDao.findOneByEmailAndPassword(email, password);
		return transformer.transformUserEntity(userEntity);
	}

	/**
	 * 
	 * @param username
	 * @param email
	 *            The email of the
	 * @return Id of the user if found or 0 is no user was found.
	 */
	public long getUserIdByUsernameOrEmail(String... credentials) {

		UserEntity entity = null;

		for (int i = 0; i < credentials.length; ++i) {
			String credential = credentials[i];

			if (credential.contains("@")) {
				entity = userDao.findOneByEmail(credential);
			} else {
				entity = userDao.findOneByUsername(credential);
			}

		}

		return entity != null ? entity.getId() : 0;
	}

}
