package com.unideb.bookshop.user.domain;

public enum UserRole {

    ROLE_ADMIN, ROLE_USER;

}
