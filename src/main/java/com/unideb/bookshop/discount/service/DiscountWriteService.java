package com.unideb.bookshop.discount.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unideb.bookshop.discount.domain.BookDiscount;
import com.unideb.bookshop.discount.repository.dao.DiscountDao;
import com.unideb.bookshop.discount.service.transform.BookDiscountTransformer;

@Service
public class DiscountWriteService {

	@Autowired
	private DiscountDao discountDao;
	@Autowired
	private BookDiscountTransformer bookDiscountTransformer;

	public void save(BookDiscount bookDiscount) {

		discountDao.save(bookDiscountTransformer.transformBookDiscount(bookDiscount));

	}

}
