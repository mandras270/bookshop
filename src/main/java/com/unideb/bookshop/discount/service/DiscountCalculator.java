package com.unideb.bookshop.discount.service;

import org.springframework.stereotype.Component;

@Component
public class DiscountCalculator {

	/**
	 * Returns the new price with the discount calculated into it. If the
	 * discount value is lesser then 0 or greater then 99 a null value will be
	 * returned.
	 * 
	 * @param price
	 *            The original price without the discount
	 * @param discount
	 *            The discount percentage
	 * @return The new price including the discount
	 */
	public Double getDiscount(double price, double discount) {

		Double result = null;

		if (discount > 0 && discount < 100) {
			double multiplier = (100 - discount) / 100;
			result = price * multiplier;
		}

		return result;
	}

}
