package com.unideb.bookshop.discount.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unideb.bookshop.discount.domain.BookDiscount;
import com.unideb.bookshop.discount.repository.dao.DiscountDao;
import com.unideb.bookshop.discount.repository.domain.BookDiscountEntity;
import com.unideb.bookshop.discount.service.transform.BookDiscountTransformer;

@Service
public class DiscountSearchService {

	@Autowired
	private DiscountDao discountDao;

	@Autowired
	private BookDiscountTransformer bookDiscountTransformer;

	public BookDiscount getBookDiscountByBookId(long bookId) {
		BookDiscountEntity entity = discountDao.findOneByBookId(bookId);

		return bookDiscountTransformer.transformBookDiscountEntity(entity);
	}
}
