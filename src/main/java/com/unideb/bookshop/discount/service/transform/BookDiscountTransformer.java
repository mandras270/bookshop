package com.unideb.bookshop.discount.service.transform;

import org.springframework.stereotype.Component;

import com.unideb.bookshop.discount.domain.BookDiscount;
import com.unideb.bookshop.discount.repository.domain.BookDiscountEntity;

@Component
public class BookDiscountTransformer {

	public BookDiscountEntity transformBookDiscount(BookDiscount bookDiscount) {

		BookDiscountEntity result = new BookDiscountEntity();
		result.setBookId(bookDiscount.getBookId());
		result.setDiscountPercentage(bookDiscount.getDiscountPercentage());

		return result;

	}

	/**
	 * Transforms a {@link BookDiscountEntity} into a {@link BookDiscount}. If
	 * the entity is null, null will be returned.
	 * 
	 * @param entity
	 *            The entity to be transformed
	 * @return A {@link BookDiscount} or null if the entity is null
	 */
	public BookDiscount transformBookDiscountEntity(BookDiscountEntity entity) {

		BookDiscount result = null;

		if (entity != null) {
			result = new BookDiscount();
			result.setBookId(entity.getBookId());
			result.setDiscountPercentage(entity.getDiscountPercentage());
		}

		return result;

	}

}
