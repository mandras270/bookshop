package com.unideb.bookshop.discount.repository.dao;

import org.springframework.data.repository.CrudRepository;

import com.unideb.bookshop.discount.repository.domain.BookDiscountEntity;

public interface DiscountDao extends CrudRepository<BookDiscountEntity, Long> {

	public BookDiscountEntity findOneByBookId(long bookId);

}
