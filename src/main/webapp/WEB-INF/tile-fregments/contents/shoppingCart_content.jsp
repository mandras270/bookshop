<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="container">
<c:choose>
<c:when test="${empty shoppingCartContent}">
<p>Sorry, your shopping cart is empty</p>
</c:when>

<c:otherwise>

<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Checkout information</div>

	<table class="table">
		<thead>
		  <tr>
		    <th>#</th>
		    <th>Title</th>
		    <th>Author</th>
		    <th>Price</th>
		    <th>Quantity</th>
		    <th>Total Price</th>
		  </tr>
		</thead>
		<tbody>
		<c:forEach var="item" items="${shoppingCartContent.items}">
			  <tr>
			    <th scope="row">${item.bookId}</th>
			    <td>${item.title}</td>
			    <td>${item.author}</td>
			    
			    <c:choose>
			    	<c:when test="${empty item.salePrice }">
			    		<td class="dollar">${item.originalPrice}</td>
			    	</c:when>
			    	<c:otherwise>
			    		<td>
			    			<span class="line-through dollar">${item.originalPrice}</span>
			    			<span class="red dollar">${item.salePrice}</span>
			    		</td>
			    	</c:otherwise>
			    </c:choose>
			    <td>${item.quantity}</td>
			    <td class="dollar">${item.totalPrice}</td>
			  </tr>
		  </c:forEach>
		</tbody>
	</table>
	<hr>
	<c:choose>
		<c:when test="${empty shoppingCartContent.regularCustomerPrice}">
			<span class="h2">Total:</span>
			<span class="h3 dollar">${shoppingCartContent.totalPrice}</span>
		</c:when>
		<c:otherwise>
			<p>
				<span class="h4 line-through">Total:</span>
				<span class="h4 line-through dollar">${shoppingCartContent.totalPrice}</span>
			</p>
			<span class="h2">Total:</span>
			<span class="h3 dollar">${shoppingCartContent.regularCustomerPrice}</span>
			<span class="h5"> (with regular customer bonus applied)</span>
		</c:otherwise>
	</c:choose>
</div>

</c:otherwise>

</c:choose>
</div>