<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:if test="${not empty message}">
  <div class="container">
    <div class="alert alert-success" role="alert">Your shopping cart has been updated.</div>
  </div>
</c:if>
<c:forEach var="book" items="${listBookModel.books}">
  <div class="row">
    <div class="book-mini-view col-sm-6 col-md-4">
      <div class="thumbnail">
      <c:if test="${book.hasCover}">
      	<c:url value="/bookCover?bookId=${book.bookId}" var="coverUrl" />
      	<img class="cover" src="${coverUrl}" alt="${book.title} by ${book.author}">
      </c:if>
        <div class="caption">
          <h3> ${book.title}</h3>
          <h4>by ${book.author}</h4>
          <p>${book.synopsis}</p>
          <p>
          <c:url value="/addToCart" var="addToCartUrl" />
          <form:form modelAttribute="addToCartRequest" method="POST" action="${addToCartUrl}">
            <form:hidden path="id" value="${book.bookId}"/>
            <form:button type="submit" class="btn btn-primary" role="button">Add to shopping cart</form:button>
          </form:form>
            <c:choose>
              <c:when test="${not empty book.salePrice}">
                  <span class="price old-price">$${book.price}</span><br>
                  <span class="price discount">$${book.salePrice}</span>
              </c:when>
              <c:otherwise>
                <span class="price">$${book.price}</span>
              </c:otherwise>
            </c:choose>
          </p>
        </div>
      </div>
    </div>
  </div>
</c:forEach>