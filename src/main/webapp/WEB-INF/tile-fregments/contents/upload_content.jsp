<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="container">

	<c:url value="/uploadBook" var="uploadBook" />
  
    <c:if test="${not empty message}">
      <div class="alert alert-success" role="alert">Book upload succeed</div>
    </c:if>
  
	<form:form enctype="multipart/form-data" modelAttribute="uploadRequest" action="${uploadBook}" role="form" class="form-signin">
  
      <h2 class="form-signin-heading">Upload new books</h2>
  
      <div class="form-group">
        <form:errors class="alert alert-danger" element="div" path="title" />
        <form:input path="title" class="form-control" placeholder="Title"/>
      </div>
      
      <div class="form-group">  
        <form:errors class="alert alert-danger" element="div" path="author" />
        <form:input path="author" class="form-control" placeholder="Author"/>
      </div>
      
      <div class="form-group">  
        <form:errors class="alert alert-danger" element="div" path="synopsis" />
        <form:input path="synopsis" class="form-control" placeholder="Synopsis"/>
      </div>
      
      <div class="form-group"> 
        <form:errors class="alert alert-danger" element="div" path="price" />
        <form:input type="number" path="price" class="form-control" placeholder="Price"/>
      </div> 
      
      <div class="form-group"> 
        <form:errors class="alert alert-danger" element="div" path="discountPercentage" />
        <form:input type="number" path="discountPercentage" class="form-control" placeholder="Discount Percentage"/>
      </div>
      
      <div class="form-group">
        <form:input type="file" path="cover" class="form-control" placeholder="Cover"/>
      </div>
      
        <button class="btn btn-lg btn-primary btn-block" type="submit">Upload</button>
      </form:form>
      
</div>