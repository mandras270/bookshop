<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<div class="jumbotron">
  <div class="container">
    <h1>Welcome to Bookshop Online!</h1>
    
    <sec:authentication property="authorities" var="roles" scope="page" />
	<span>Your roles are:</span>
	<ul>
	    <c:forEach var="role" items="${roles}">
	    <li>${role}</li>
	    </c:forEach>
	</ul>
    
    <p>Please, take a moment to browse our extra ordinary book collection</p>
    <p><a class="btn btn-primary btn-lg" href='<c:url value="/browse"/>' role="button">Browse &raquo;</a></p>
  </div>
</div>