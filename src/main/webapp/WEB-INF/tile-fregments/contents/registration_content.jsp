<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="container">

	<c:url value="/saveUser" var="saveUserUrl" />
	
	<c:choose>
		<c:when test="${success && not empty message}">
			<div class="alert alert-success">${message}</div>
		</c:when>
		<c:when test="${not success && not empty message}">
			<div class="alert alert-danger">${message}</div>
		</c:when>
	</c:choose>
	<form:form modelAttribute="registrationRequest" class="form-signin" action="${saveUserUrl}">
    	<h2 class="form-signin-heading">Please register</h2>
    	
    	<form:errors path="username" element="div" cssClass="alert alert-danger" />
        <label for="username" class="sr-only">Username</label>
        <form:input path="username" class="form-control" placeholder="Username" />
        
        <form:errors path="email" element="div" cssClass="alert alert-danger" />
        <label for="email" class="sr-only">E-mail</label>
        <form:input path="email" class="form-control" placeholder="E-mail" />
        
        <form:errors path="password" element="div" cssClass="alert alert-danger" />
        <label for="password" class="sr-only">Password</label>
        <form:password path="password" class="form-control" placeholder="Password" />
        
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form:form>
</div>