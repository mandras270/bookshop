<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="container">
	<c:url value="j_spring_security_check" var="loginUrl" />
  
	<form:form modelAttribute="loginRequest" class="form-signin" action="${loginUrl}">
  
		<form:errors element="div" cssClass="alert alert-danger" />
    	<h2 class="form-signin-heading">Please sign in</h2>
        <label for="username" class="sr-only">Email address or Username</label>
        <form:input path="username" class="form-control" placeholder="Email address or Username" />
        <label for="password" class="sr-only">Password</label>
        <form:password path="password" class="form-control" placeholder="Password" />
        
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        
      </form:form>
</div>