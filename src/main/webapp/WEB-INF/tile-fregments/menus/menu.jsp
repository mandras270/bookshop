<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <c:url value="/" var="homepage" />
      <a class="navbar-brand" href="${homepage}">Bookshop Online</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
      
        <li><a href="${homepage}">Home<span class="sr-only">(current)</span></a></li>
        
        <li><a href="<c:url value="/browse" />">Browse</a></li>
        
        <c:if test="${not authentication.userAuthenticated}">
       		<li><a href="<c:url value="/registration" />">Register</a></li>
        </c:if>
        
        <c:if test="${authentication.admin}">
	        <c:url value="/upload" var="upload" />
	        <li><a href="${upload}">Upload</a></li>
        </c:if>
        
        
        <c:url value="/checkout" var="checkout" />
        <li><a href="${checkout}">View Shopping Cart (${sessionScope.SHOPPING_CART.length})</a></li>
      </ul>
       <c:url value="/browse" var="searchUrl" />
      <form:form modelAttribute="searchModel" class="navbar-form navbar-left" role="search" action="${searchUrl}">
        <div class="form-group">
          <form:input path="title" type="text" class="form-control" placeholder="Search" />
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form:form>
      
      <c:choose>
      <c:when test="${authentication.userAuthenticated}">
      	<c:url value="/logout" var="logoutUrl"/>
      	<form:form action="${logoutUrl}">
      		<button class="btn btn-default navbar-btn navbar-right" type="submit">Sign out</button>
      	</form:form>
      </c:when>
      <c:otherwise>
      	<c:url value="/login" var="loginUrl" />
      	<a class="btn btn-default navbar-btn navbar-right" href="${loginUrl}">Sign in</a>
      </c:otherwise>
      </c:choose>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>