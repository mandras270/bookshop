<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><tiles:insertAttribute name="title" /></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href='<c:url value="/assets/css/bootstrap.min.css" />'>
	<link rel="stylesheet" href='<c:url value="/assets/css/bootstrap-theme.min.css" />'>
    <link rel="stylesheet" href='<c:url value="/assets/css/main.css" />'>
</head>
<body>
	<tiles:insertAttribute name="menu" />
	<tiles:insertAttribute name="content" />
	<tiles:insertAttribute name="footer" />
</body>
</html>