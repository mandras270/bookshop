insert into UserEntity (id, email, enabled, password, username) values (1, 'admin@admin.com', 'true', 'admin', 'admin');
insert into UserEntity_userRoles (UserEntity_id, userRoles) values (1, 'ROLE_USER');
insert into UserEntity_userRoles (UserEntity_id, userRoles) values (1, 'ROLE_ADMIN');

insert into UserEntity (id, email, enabled, password, username) values (2, 'user@user.com', 'true', 'user', 'user');
insert into UserEntity_userRoles (UserEntity_id, userRoles) values (2, 'ROLE_USER');

insert into RegularCustomerTokenEntity (id, userId) values(default, 2);

insert into books (id, price, author, synopsis, title) values(1, 10.0, 'J. K. Rowling', 'Before the start of the novel, Lord Voldemort, the most evil and powerful dark wizard in history, murders married couple James and Lily Potter but mysteriously disappears after attempting to kill their infant son, Harry. While the wizarding world celebrates Voldemort''s downfall, Professor Dumbledore, Professor McGonagall and half-giant Rubeus Hagrid place the one-year-old orphan in the care of his surly and cold Muggle (non-magic human) uncle and aunt, Vernon and Petunia Dursley, with their spoiled and bullying son, Dudley.', 'Harry Potter and the Philosopher''s Stone');
insert into books (id, price, author, synopsis, title) values(2, 10.0, 'J. K. Rowling', 'Harry Potter and the Chamber of Secrets is the second novel in the Harry Potter series, written by J. K. Rowling. The plot follows Harry''s second year at Hogwarts School of Witchcraft and Wizardry, during which a series of messages on the walls of the school''s corridors warn that the "Chamber of Secrets" has been opened and that the "heir of Slytherin" would kill all pupils who do not come from all-magical families. These threats are followed by attacks which leave residents of the school "petrified" (frozen like stone). Throughout the year, Harry and his friends Ron Weasley and Hermione Granger investigate the attacks.', 'Harry Potter and the Chamber of Secrets');
insert into books (id, price, author, synopsis, title) values(3, 10.0, 'J. K. Rowling', 'Harry Potter and the Prisoner of Azkaban is the third novel in the Harry Potter series, written by J. K. Rowling. The book follows Harry Potter, a young wizard, in his third year at Hogwarts School of Witchcraft and Wizardry. Along with friends Ron Weasley and Hermione Granger, Harry investigates Sirius Black, an escaped prisoner from Azkaban whom they believe is one of Lord Voldemort''s old allies.', 'Harry Potter and the Prisoner of Azkaban');
insert into books (id, price, author, synopsis, title) values(4, 10.0, 'J. K. Rowling', 'Harry Potter and the Goblet of Fire is the fourth novel in the Harry Potter series, written by British author J. K. Rowling. It follows Harry Potter, a wizard in his fourth year at Hogwarts School of Witchcraft and Wizardry and the mystery surrounding the entry of Harry''s name into the Triwizard Tournament, in which he is forced to compete.', 'Harry Potter and the Goblet of Fire');

insert into BookDiscountEntity (id, bookId, discountPercentage) values (default, 1, 15);

insert into CuponEntity (id, code, discountPercentage ) values(default, 'NXNX8C', 20);
insert into CuponEntity (id, code, discountPercentage ) values(default, 'NXNX8B', 5);